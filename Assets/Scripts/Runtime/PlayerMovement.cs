using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float speed;
    bool moveRight;
    bool moveLeft;
    bool stopMoving;
    public Collider2D wall;
    float boundary;

    // Start is called before the first frame update
    void Start()
    {
        speed = 3.0f;
        moveRight = false;
        moveRight = false;
        stopMoving = true;
        boundary = wall.offset.x - (wall.bounds.size.x / 2) - (gameObject.GetComponent<Collider2D>().bounds.size.x / 2) - 0.01f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!stopMoving)
        {
            if (moveRight)
                transform.Translate(Vector3.right * speed * Time.fixedDeltaTime);

            if (moveLeft)
                transform.Translate(Vector3.left * speed * Time.fixedDeltaTime);
        }

        if (gameObject.transform.position.x > boundary)
        {
            transform.position = new Vector2(boundary - 0.01f, transform.position.y);
            moveRight = false;
        }

        if (gameObject.transform.position.x < -boundary)
        {
            transform.position = new Vector2(-(boundary - 0.01f), transform.position.y);
            moveLeft = false;
        }
    }

    public void MoveRight()
    {
        stopMoving = false;
        moveRight = true;
    }

    public void MoveLeft()
    {
        stopMoving = false;
        moveLeft = true;
    }

    public void StopMoving()
    {
        stopMoving = true;
        moveLeft = false;
        moveRight = false;
    }
}

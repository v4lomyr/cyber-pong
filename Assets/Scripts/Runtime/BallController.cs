using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    private Rigidbody2D rb2D;
    public float force;
    static int scoreCount; 

    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        rb2D.AddForce(new Vector2(1, -1) * force);
        scoreCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static int getPointCount()
    {
        return scoreCount;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if (collision.gameObject.CompareTag("Player"))
        //{
        //    scoreCount++;
        //    rb2D.velocity = new Vector2(0, 0);
        //    rb2D.AddForce(Vector2.up * force);
        //}

        //if (collision.gameObject.CompareTag("Enemy"))
        //{
        //    scoreCount++;
        //    rb2D.velocity = new Vector2(0, 0);
        //    rb2D.AddForce(Vector2.down * force);
        //}
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Goal"))
        {
            scoreCount = 0;
            transform.position = new Vector2(0, 0);
        }
    }
}
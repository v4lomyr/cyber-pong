using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance { get; private set; }

    private static int playerHp;
    private int enemyHp;

    public HealthBar playerHealthBar;
    public HealthBar enemyHealthBar;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        playerHp = 100;
        enemyHp = 100;

        playerHealthBar.setMaxHealth(playerHp);
        enemyHealthBar.setMaxHealth(enemyHp);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void reducePlayerHP(int point)
    {
        playerHp -= point;
        playerHealthBar.setHealth(playerHp);
    }

    public void reduceEnemyHP(int point)
    {
        enemyHp -= point;
        enemyHealthBar.setHealth(enemyHp);
    }
}
